#!/bin/bash

set -e

echo "deb http://deb.debian.org/debian bullseye-backports main" >> /etc/apt/sources.list
apt-get update
apt-get -y dist-upgrade
apt-get -y install lsb-release wget software-properties-common gnupg git
apt-get -y install -t bullseye-backports cmake

wget https://apt.llvm.org/llvm.sh
chmod +x llvm.sh
./llvm.sh 17 all

git clone https://github.com/include-what-you-use/include-what-you-use.git iwyu
cd iwyu
git checkout clang_17
mkdir build && cd build
cmake -DBUILD_SHARED_LIBS=ON -DCMAKE_PREFIX_PATH=/usr/lib/llvm-17 ..
make -j
make -j install

cd ../..
rm -rf iwyu

