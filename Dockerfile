# syntax=docker/dockerfile:1

FROM debian:11
COPY ./build.sh /build.sh
RUN /build.sh
